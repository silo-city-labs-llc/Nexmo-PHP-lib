<?php

/**
 * Class NexmoVoice handles the methods and properties of sending an voice calls.
 * 
 * Usage: $var = new NexmoVoice ( $api_key, $api_secret );
 * Methods:
 *     sendTTSCall ( $to, $from, $message )
 *
 */

class NexmoVoice {

	// Nexmo account credentials
	private $nx_key = '';
	private $nx_secret = '';

	/**
	 * @var string Nexmo server URI
	 *
	 * We're sticking with the JSON interface here since json
	 * parsing is built into PHP and requires no extensions.
	 * This will also keep any debugging to a minimum due to
	 * not worrying about which parser is being used.
	 */
	var $nx_uri = 'https://rest.nexmo.com/tts/json';

	
	/**
	 * @var array The most recent parsed Nexmo response.
	 */
	public $nexmo_response = '';

	// A few options
	public $ssl_verify = false; // Verify Nexmo SSL before sending any message


	function NexmoVoice ($api_key, $api_secret) {
		$this->nx_key = $api_key;
		$this->nx_secret = $api_secret;
	}



	/**
	 * Prepare new text message.
	 *
	 * If $unicode is not provided we will try to detect the
	 * message type. Otherwise set to TRUE if you require
	 * unicode characters.
	 */
	function sendTTSCall ( $to, $from, $message ) {
	
		// Making sure strings are UTF-8 encoded
		if ( !is_numeric($from) && !mb_check_encoding($from, 'UTF-8') ) {
			trigger_error('$from needs to be a valid UTF-8 encoded string');
			return false;
		}

		if ( !mb_check_encoding($message, 'UTF-8') ) {
			trigger_error('$message needs to be a valid UTF-8 encoded string');
			return false;
		}
		
		if(strlen($message) > 1000){
			trigger_error('$message is to long, must be less than 1000 characters');
			return false;
		}
		
		// Make sure $from is valid
		$from = $this->validateOriginator($from);

		// URL Encode
		$from = urlencode( $from );
		$message = urlencode( $message );
		
		// Send away!
		$post = array(
			'from' => $from,
			'to' => $to,
			'text' => $message
		);
		return $this->sendRequest ( $post );
		
	}
	
	
	/**
	 * Prepare and send a new message.
	 */
	private function sendRequest ( $data ) {
		// Build the post data
		$data = array_merge($data, array('api_key' => $this->nx_key, 'api_secret' => $this->nx_secret));
		$post = '';
		foreach($data as $k => $v){
			$post .= "&$k=$v";
		}
		
		echo "<br>".$post."<br>";

		// If available, use CURL
		if (function_exists('curl_version')) {

			$to_nexmo = curl_init( $this->nx_uri );
			curl_setopt( $to_nexmo, CURLOPT_POST, true );
			curl_setopt( $to_nexmo, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $to_nexmo, CURLOPT_POSTFIELDS, $post );

			if (!$this->ssl_verify) {
				curl_setopt( $to_nexmo, CURLOPT_SSL_VERIFYPEER, false);
			}

			$from_nexmo = curl_exec( $to_nexmo );
			curl_close ( $to_nexmo );

		} elseif (ini_get('allow_url_fopen')) {
			// No CURL available so try the awesome file_get_contents

			$opts = array('http' =>
				array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => $post
				)
			);
			$context = stream_context_create($opts);
			$from_nexmo = file_get_contents($this->nx_uri, false, $context);

		} else {
			// No way of sending a HTTP post :(
			return false;
		}

		
		return $this->nexmoParse( $from_nexmo );
	 
	}
	
	
	/**
	 * Recursively normalise any key names in an object, removing unwanted characters
	 */
	private function normaliseKeys ($obj) {
		// Determine is working with a class or araay
		if ($obj instanceof stdClass) {
			$new_obj = new stdClass();
			$is_obj = true;
		} else {
			$new_obj = array();
			$is_obj = false;
		}


		foreach($obj as $key => $val){
			// If we come across another class/array, normalise it
			if ($val instanceof stdClass || is_array($val)) {
				$val = $this->normaliseKeys($val);
			}
			
			// Replace any unwanted characters in they key name
			if ($is_obj) {
				$new_obj->{str_replace('-', '', $key)} = $val;
			} else {
				$new_obj[str_replace('-', '', $key)] = $val;
			}
		}

		return $new_obj;
	}


	/**
	 * Parse server response.
	 */
	private function nexmoParse ( $from_nexmo ) {
		$response = json_decode($from_nexmo);

		// Copy the response data into an object, removing any '-' characters from the key
		$response_obj = $this->normaliseKeys($response);

		if ($response_obj) {
			$this->nexmo_response = $response_obj;

			// Find the total cost of this message
			$response_obj->cost = $total_cost = 0;
			if (is_array($response_obj->messages)) {
				foreach ($response_obj->messages as $msg) {
					if (property_exists($msg, "messageprice")) {
						$total_cost = $total_cost + (float)$msg->messageprice;
					}
				}

				$response_obj->cost = $total_cost;
			}

			return $response_obj;

		} else {
			// A malformed response
			$this->nexmo_response = array();
			return false;
		}
		
	}


	/**
	 * Validate an originator string
	 *
	 * If the originator ('from' field) is invalid, some networks may reject the network
	 * whilst stinging you with the financial cost! While this cannot correct them, it
	 * will try its best to correctly format them.
	 */
	private function validateOriginator($inp){
		// Remove any invalid characters
		$ret = preg_replace('/[^a-zA-Z0-9]/', '', (string)$inp);

		if(preg_match('/[a-zA-Z]/', $inp)){

			// Alphanumeric format so make sure it's < 11 chars
			$ret = substr($ret, 0, 11);

		} else {

			// Numerical, remove any prepending '00'
			if(substr($ret, 0, 2) == '00'){
				$ret = substr($ret, 2);
				$ret = substr($ret, 0, 15);
			}
		}
		
		return (string)$ret;
	}
}
